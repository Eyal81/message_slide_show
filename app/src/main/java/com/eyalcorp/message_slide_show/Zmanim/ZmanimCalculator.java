package com.eyalcorp.message_slide_show.Zmanim;

import com.kosherjava.zmanim.AstronomicalCalendar;
import com.kosherjava.zmanim.ZmanimCalendar;
import com.kosherjava.zmanim.util.GeoLocation;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class ZmanimCalculator {

    public static String calculateSunriseOhrHaChaim(double latitude, double longitude) {
        AstronomicalCalendar astronomicalCalendar = new AstronomicalCalendar();
        astronomicalCalendar.setGeoLocation(new GeoLocation("City", latitude, longitude, 0, TimeZone.getTimeZone("Asia/Jerusalem")));

        // Calculate sunrise according to Ohr HaChaim opinion
        Date sunrise = astronomicalCalendar.getSunrise();
        return formatTime(sunrise);
    }

    public static String calculateSunsetOhrHaChaim(double latitude, double longitude) {
        AstronomicalCalendar astronomicalCalendar = new AstronomicalCalendar();
        astronomicalCalendar.setGeoLocation(new GeoLocation("City", latitude, longitude, 0, TimeZone.getTimeZone("Asia/Jerusalem")));

        // Calculate sunset according to Ohr HaChaim opinion
        Date sunset = astronomicalCalendar.getSunset();
        return formatTime(sunset);
    }

    public static String calculateTzaitHakochavim(double latitude, double longitude) {
        AstronomicalCalendar astronomicalCalendar = new AstronomicalCalendar();
        astronomicalCalendar.setGeoLocation(new GeoLocation("City", latitude, longitude, 0, TimeZone.getTimeZone("Asia/Jerusalem")));

        // Calculate Tzait HaKochavim
        Date tzaitHakochavim = astronomicalCalendar.getSeaLevelSunset();
        return formatTime(tzaitHakochavim);
    }

    public static String calculateSofZmanKriatShemaMGA(double latitude, double longitude) {
        ZmanimCalendar zmanimCalendar = new ZmanimCalendar();
        zmanimCalendar.setGeoLocation(new GeoLocation("City", latitude, longitude, 0, TimeZone.getTimeZone("Asia/Jerusalem")));

        // Calculate Sof Zman Kriat Shema (MGA)
        Date sofZmanKriatShemaMGA = zmanimCalendar.getSofZmanShmaMGA();
        return formatTime(sofZmanKriatShemaMGA);
    }

    public static String calculateSofZmanKriatShemaGra(double latitude, double longitude) {
        ZmanimCalendar zmanimCalendar = new ZmanimCalendar();
        zmanimCalendar.setGeoLocation(new GeoLocation("City", latitude, longitude, 0, TimeZone.getTimeZone("Asia/Jerusalem")));

        // Calculate Sof Zman Kriat Shema (Gra)
        Date sofZmanKriatShema = zmanimCalendar.getSofZmanShmaGRA();
        return formatTime(sofZmanKriatShema);
    }

    public static String calculateMinchaKetana(double latitude, double longitude) {
        ZmanimCalendar zmanimCalendar = new ZmanimCalendar();
        zmanimCalendar.setGeoLocation(new GeoLocation("City", latitude, longitude, 0, TimeZone.getTimeZone("Asia/Jerusalem")));

        // Calculate Mincha Ketana
        Date minchaKetana = zmanimCalendar.getMinchaKetana();
        return formatTime(minchaKetana);
    }

    private static String formatTime(Date time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        return dateFormat.format(time);
    }
}
