package com.eyalcorp.message_slide_show.Zmanim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CityInfo {
    private static Map<String, CityData> cityDataMap = new HashMap<>();

    static {
        // Populate the map with city data for major cities in Israel
        cityDataMap.put("jerusalem", new CityData("Asia/Jerusalem", 31.7683, 35.2137));
        cityDataMap.put("tel aviv", new CityData("Asia/Jerusalem", 32.0853, 34.7818));
        cityDataMap.put("haifa", new CityData("Asia/Jerusalem", 32.7940, 34.9896));
        cityDataMap.put("beer sheva", new CityData("Asia/Jerusalem", 31.2518, 34.7913));
        // Add more cities and data as needed
    }

    public static CityData getCityInfo(String cityName) {
        CityData cityData = cityDataMap.get(cityName.toLowerCase());
        return cityData;
    }

    public static List<String> getCities(){
        return new ArrayList<>(cityDataMap.keySet());
    }

    public static class CityData {
        private String timeZone;
        private double latitude;
        private double longitude;

        public CityData(String timeZone, double latitude, double longitude) {
            this.timeZone = timeZone;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public String getTimeZone() {
            return timeZone;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }
    }

}
