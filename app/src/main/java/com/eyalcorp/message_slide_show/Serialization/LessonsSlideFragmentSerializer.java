package com.eyalcorp.message_slide_show.Serialization;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.eyalcorp.message_slide_show.Slides.LessonsSlideFragment;
import com.google.gson.*;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;

public class LessonsSlideFragmentSerializer implements JsonSerializer<LessonsSlideFragment>, JsonDeserializer<LessonsSlideFragment> {
    @Override
    public JsonElement serialize(LessonsSlideFragment src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("lessonText", src.lessonText);
        jsonObject.addProperty("textColor", src.textColor);
        jsonObject.addProperty("msgPos", src.msgPos.toString());

        // Serialize the path to the background image file
        if (src.backgroundImagePath != null) {
            jsonObject.addProperty("backgroundImagePath", src.backgroundImagePath);
        }


        return jsonObject;
    }

    @Override
    public LessonsSlideFragment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        String lessonText = jsonObject.get("lessonText").getAsString();
        int textColor = jsonObject.get("textColor").getAsInt();
        String msgPosStr = jsonObject.get("msgPos").getAsString();
        LessonsSlideFragment.MsgPos msgPos = LessonsSlideFragment.MsgPos.valueOf(msgPosStr);

        // Deserialize the backgroundImagePath
        String backgroundImagePath = jsonObject.get("backgroundImagePath").getAsString();

        // Load the image from the stored path
        Bitmap backgroundImage = BitmapFactory.decodeFile(backgroundImagePath);

        return new LessonsSlideFragment(lessonText, textColor, msgPos, backgroundImagePath);
    }

}
