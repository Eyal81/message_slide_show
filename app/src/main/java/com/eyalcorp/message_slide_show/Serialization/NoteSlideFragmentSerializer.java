package com.eyalcorp.message_slide_show.Serialization;

import com.eyalcorp.message_slide_show.Slides.NoteSlideFragment;
import com.google.gson.*;
import java.lang.reflect.Type;

public class NoteSlideFragmentSerializer implements JsonSerializer<NoteSlideFragment>, JsonDeserializer<NoteSlideFragment> {
    @Override
    public JsonElement serialize(NoteSlideFragment src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("title", src.title);
        jsonObject.addProperty("text", src.text);
        // You can add more fields to serialize here
        return jsonObject;
    }

    @Override
    public NoteSlideFragment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        String title = jsonObject.get("title").getAsString();
        String text = jsonObject.get("text").getAsString();
        // You can extract more fields and create a new NoteSlideFragment instance here
        return new NoteSlideFragment(title, text);
    }
}
