package com.eyalcorp.message_slide_show.Serialization;

import com.eyalcorp.message_slide_show.Slides.BirthdaySlideFragment;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;


public class BirthdaySlideFragmentSerializer implements JsonSerializer<BirthdaySlideFragment>, JsonDeserializer<BirthdaySlideFragment> {
    @Override
    public JsonElement serialize(BirthdaySlideFragment src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("childName", src.childName);
        jsonObject.addProperty("birthdayDate", src.birthdayDate);
        return jsonObject;
    }

    @Override
    public BirthdaySlideFragment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        String childName = jsonObject.get("childName").getAsString();
        String birthdayDate = jsonObject.get("birthdayDate").getAsString();
        return new BirthdaySlideFragment(childName, birthdayDate);
    }
}

