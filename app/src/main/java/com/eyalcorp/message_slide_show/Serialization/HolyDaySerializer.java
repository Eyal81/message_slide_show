package com.eyalcorp.message_slide_show.Serialization;

import com.eyalcorp.message_slide_show.Slides.HolyDaysFragment;
import com.eyalcorp.message_slide_show.Utils;
import com.google.gson.*;
import java.lang.reflect.Type;

public class HolyDaySerializer implements JsonSerializer<HolyDaysFragment.HolyDay>, JsonDeserializer<HolyDaysFragment.HolyDay> {
    @Override
    public JsonElement serialize(HolyDaysFragment.HolyDay src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("background", src.background);
        jsonObject.addProperty("blessing", src.blessing);
        jsonObject.addProperty("subBlessing", src.subBlessing);
        jsonObject.addProperty("blessingColor", src.blessingColor);
        jsonObject.add("blessingMargin", context.serialize(src.blessingMargin));
        return jsonObject;
    }

    @Override
    public HolyDaysFragment.HolyDay deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        int background = jsonObject.get("background").getAsInt();
        String blessing = jsonObject.get("blessing").getAsString();
        String subBlessing = jsonObject.get("subBlessing").getAsString();
        int blessingColor = jsonObject.get("blessingColor").getAsInt();
        Utils.Margin margin = context.deserialize(jsonObject.get("blessingMargin"), Utils.Margin.class);
        return new HolyDaysFragment.HolyDay(background, blessing, subBlessing, blessingColor, margin);
    }
}
