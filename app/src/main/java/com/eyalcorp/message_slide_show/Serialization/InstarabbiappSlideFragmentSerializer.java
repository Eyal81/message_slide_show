package com.eyalcorp.message_slide_show.Serialization;

import com.eyalcorp.message_slide_show.Slides.InstarabbiappSlideFragment;
import com.google.gson.*;
import java.lang.reflect.Type;

public class InstarabbiappSlideFragmentSerializer implements JsonSerializer<InstarabbiappSlideFragment>, JsonDeserializer<InstarabbiappSlideFragment> {
    @Override
    public JsonElement serialize(InstarabbiappSlideFragment src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        // Add serialization logic for InstarabbiappSlideFragment fields here
        return jsonObject;
    }

    @Override
    public InstarabbiappSlideFragment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        // Extract values from jsonObject and create a new InstarabbiappSlideFragment instance
        return new InstarabbiappSlideFragment();
    }
}

