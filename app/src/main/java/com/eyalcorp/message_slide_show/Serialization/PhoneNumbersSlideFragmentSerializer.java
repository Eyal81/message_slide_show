package com.eyalcorp.message_slide_show.Serialization;

import com.eyalcorp.message_slide_show.Slides.PhoneNumbersSlideFragment;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class PhoneNumbersSlideFragmentSerializer implements JsonSerializer<PhoneNumbersSlideFragment>, JsonDeserializer<PhoneNumbersSlideFragment> {
    @Override
    public JsonElement serialize(PhoneNumbersSlideFragment src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        // You can add more fields to serialize here
        return jsonObject;
    }

    @Override
    public PhoneNumbersSlideFragment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        // You can extract fields and create a new PhoneNumbersSlideFragment instance here
        return new PhoneNumbersSlideFragment();
    }
}
