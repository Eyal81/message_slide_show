package com.eyalcorp.message_slide_show.Serialization;

import com.eyalcorp.message_slide_show.Slides.HolyDaysFragment;
import com.eyalcorp.message_slide_show.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class HolyDaysFragmentSerializer implements JsonSerializer<HolyDaysFragment>, JsonDeserializer<HolyDaysFragment> {
    private final Gson gson;

    public HolyDaysFragmentSerializer() {
        // Create a Gson instance with custom adapters
        gson = new GsonBuilder()
                .registerTypeAdapter(HolyDaysFragment.HolyDay.class, new HolyDaySerializer())
                .registerTypeAdapter(Utils.Margin.class, new MarginTypeAdapter())
                .create();
    }

    @Override
    public JsonElement serialize(HolyDaysFragment src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("holyDay", gson.toJsonTree(src.getHolyDay()));
        return jsonObject;
    }

    @Override
    public HolyDaysFragment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        JsonObject holyDayObject = jsonObject.getAsJsonObject("holyDay");
        HolyDaysFragment.HolyDay holyDay = gson.fromJson(holyDayObject, HolyDaysFragment.HolyDay.class);
        return new HolyDaysFragment(holyDay);
    }
}

