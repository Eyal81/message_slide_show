package com.eyalcorp.message_slide_show.Serialization;

import com.eyalcorp.message_slide_show.Utils;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class MarginTypeAdapter extends TypeAdapter<Utils.Margin> {
    @Override
    public void write(JsonWriter out, Utils.Margin margin) throws IOException {
        if (margin == null) {
            out.nullValue();
            return;
        }

        out.beginObject();
        out.name("left").value(margin.l);
        out.name("top").value(margin.t);
        out.name("right").value(margin.r);
        out.name("bottom").value(margin.b);
        out.endObject();
    }

    @Override
    public Utils.Margin read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        }

        in.beginObject();
        int left = 0, top = 0, right = 0, bottom = 0;

        while (in.hasNext()) {
            String name = in.nextName();
            switch (name) {
                case "left":
                    left = in.nextInt();
                    break;
                case "top":
                    top = in.nextInt();
                    break;
                case "right":
                    right = in.nextInt();
                    break;
                case "bottom":
                    bottom = in.nextInt();
                    break;
                default:
                    in.skipValue(); // Ignore unknown properties
                    break;
            }
        }

        in.endObject();
        return new Utils.Margin(left, top, right, bottom);
    }
}
