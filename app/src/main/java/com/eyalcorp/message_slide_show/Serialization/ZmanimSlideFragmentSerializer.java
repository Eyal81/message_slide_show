package com.eyalcorp.message_slide_show.Serialization;

import com.eyalcorp.message_slide_show.Slides.ZmanimSlideFragment;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class ZmanimSlideFragmentSerializer implements JsonSerializer<ZmanimSlideFragment>, JsonDeserializer<ZmanimSlideFragment> {
    @Override
    public JsonElement serialize(ZmanimSlideFragment src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("cityName", src.getCityName());
        // You can add more fields to serialize here
        return jsonObject;
    }

    @Override
    public ZmanimSlideFragment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        String cityName = jsonObject.get("cityName").getAsString();
        // You can extract more fields and create a new ZmanimSlideFragment instance here
        return new ZmanimSlideFragment(cityName);
    }
}

