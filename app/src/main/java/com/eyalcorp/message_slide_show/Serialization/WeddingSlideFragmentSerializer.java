package com.eyalcorp.message_slide_show.Serialization;

import com.eyalcorp.message_slide_show.Slides.WeddingSlideFragment;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class WeddingSlideFragmentSerializer implements JsonSerializer<WeddingSlideFragment>, JsonDeserializer<WeddingSlideFragment> {
    @Override
    public JsonElement serialize(WeddingSlideFragment src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("brideName", src.brideName);
        // You can add more fields to serialize here
        return jsonObject;
    }

    @Override
    public WeddingSlideFragment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        String brideName = jsonObject.get("brideName").getAsString();
        // You can extract more fields and create a new WeddingSlideFragment instance here
        return new WeddingSlideFragment(brideName);
    }
}
