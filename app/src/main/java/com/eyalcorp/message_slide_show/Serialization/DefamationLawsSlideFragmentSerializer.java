package com.eyalcorp.message_slide_show.Serialization;

import com.eyalcorp.message_slide_show.Slides.DefamationLawsSlideFragment;
import com.google.gson.*;
import java.lang.reflect.Type;

public class DefamationLawsSlideFragmentSerializer implements JsonSerializer<DefamationLawsSlideFragment>, JsonDeserializer<DefamationLawsSlideFragment> {
    @Override
    public JsonElement serialize(DefamationLawsSlideFragment src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        // Add serialization logic for DefamationLawsSlideFragment fields here
        return jsonObject;
    }

    @Override
    public DefamationLawsSlideFragment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        // Extract values from jsonObject and create a new DefamationLawsSlideFragment instance
        return new DefamationLawsSlideFragment();
    }
}
