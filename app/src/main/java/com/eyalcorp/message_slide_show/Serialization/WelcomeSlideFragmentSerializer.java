package com.eyalcorp.message_slide_show.Serialization;

import com.eyalcorp.message_slide_show.Slides.WelcomeSlideFragment;
import com.google.gson.*;
import java.lang.reflect.Type;

public class WelcomeSlideFragmentSerializer implements JsonSerializer<WelcomeSlideFragment>, JsonDeserializer<WelcomeSlideFragment> {
    @Override
    public JsonElement serialize(WelcomeSlideFragment src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        // You can add more fields to serialize here
        return jsonObject;
    }

    @Override
    public WelcomeSlideFragment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        // You can extract fields and create a new WelcomeSlideFragment instance here
        return new WelcomeSlideFragment();
    }
}
