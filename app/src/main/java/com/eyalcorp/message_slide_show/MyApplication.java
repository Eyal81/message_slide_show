package com.eyalcorp.message_slide_show;

import android.app.Application;

import com.eyalcorp.message_slide_show.Slides.SlideFragment;

import java.util.ArrayList;

public class MyApplication extends Application {
    ArrayList<SlideFragment> slides = new ArrayList<>();

    public MyApplication() {

    }

    public ArrayList<SlideFragment> getSlides() {
        return slides;
    }

    public void setSlides(ArrayList<SlideFragment> slides) {
        this.slides = slides;
    }
}
