package com.eyalcorp.message_slide_show.Slides;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.eyalcorp.messageslideshow.R;

public class InstarabbiappSlideFragment extends SlideFragment {
    public InstarabbiappSlideFragment() {
        this.SlideTimeMS = 10000;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.instarabbiapp_fragment_slide, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get the VideoView from the layout
        VideoView videoView = view.findViewById(R.id.videoView);

        // Set the video file's path using the resource ID
        String videoPath = "android.resource://" + getActivity().getPackageName()
                + "/" + R.raw.video;
        Uri uri = Uri.parse(videoPath);

        // Create a MediaController for video playback controls
        MediaController mediaController = new MediaController(getContext());
        videoView.setMediaController(mediaController);

        // Set the video URI and start playback
        videoView.setVideoURI(uri);
        videoView.start();

        // Set an OnCompletionListener to loop the video
        videoView.setOnCompletionListener(mp -> {
            // Restart the video when it reaches the end
            videoView.seekTo(0);
            videoView.start();
        });

    }
}
