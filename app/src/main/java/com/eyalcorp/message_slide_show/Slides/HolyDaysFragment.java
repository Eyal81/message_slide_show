package com.eyalcorp.message_slide_show.Slides;

import static com.eyalcorp.message_slide_show.Utils.setMargins;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.eyalcorp.message_slide_show.Utils;
import com.eyalcorp.messageslideshow.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class HolyDaysFragment extends SlideFragment {

    HolyDay holyDay;

    public static int DIALOG_HOLY_DAY_ID = View.generateViewId();

    public HolyDaysFragment(HolyDay holyDay) {
        this.holyDay = holyDay;
    }

    public static final HolyDay[] HOLY_DAYS = {
            new HolyDay(R.drawable.rosh_hashanah_bg, "שנה טובה!", "", 0xFFFFFF00, new Utils.Margin(0, 0, 0, 0)),
            new HolyDay(R.drawable.hanukkah_bg, "חנוכה שמח!", "", 0xeb3434FF, new Utils.Margin(0, 0, 0, 0)),
            new HolyDay(R.drawable.fasting_bg, "צום מועיל!", "שיבנה בית המקדש במהרה בימנו!", 0xFF000000, new Utils.Margin(0, 0, 0, 0)),
            new HolyDay(R.drawable.lag_bomer_bg, "ל\"ג בעומר שמח!", "", 0xFFFFFFFF, new Utils.Margin(0, 0, 100, 0)),
            new HolyDay(R.drawable.passover_bg, "חג פסח\nשמח וכשר!", "", 0xFFFFFFFF, new Utils.Margin(0, 0, 300, 0)),
            new HolyDay(R.drawable.puirm_bg, "פורים שמח ומבדח!", "", 0xFFFFFFFF, new Utils.Margin(0, 0, 0, 0)),
            new HolyDay(R.drawable.shavuot_bg, "חג שבועות שמח!", "", 0xFF000000, new Utils.Margin(0, 0, 0, 450)),
            new HolyDay(R.drawable.sukkot_bg, "חג סוכות שמח!", "", 0xeb3434FF, new Utils.Margin(0, 0, 0, 0)),
            new HolyDay(R.drawable.tu_bishvat_bg, "ט\"ו בשבט\nשמח!", "", 0xFF000000, new Utils.Margin(0, 150, 550, 0))
    };


    public static class HolyDay {
        public int background;
        public String blessing;
        public String subBlessing;
        public int blessingColor;

        public Utils.Margin blessingMargin;

        public HolyDay() {
        }

        // Deserialize JSON into a HolyDay object
        public static HolyDay fromJson(String json) {
            Gson gson = new Gson();
            return gson.fromJson(json, HolyDay.class);
        }

        public HolyDay(int background, String blessing, String subBlessing, int blessingColor, Utils.Margin blessingMargin) {
            this.background = background;
            this.blessing = blessing;
            this.subBlessing = subBlessing;
            this.blessingColor = blessingColor;
            this.blessingMargin = blessingMargin;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.holy_days_fragment_slide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.holy_day_layout).setBackgroundResource(holyDay.background);
        TextView blessing = view.findViewById(R.id.blessing);
        blessing.setText(holyDay.blessing);
        blessing.setTextColor(holyDay.blessingColor);
        setMargins(blessing, holyDay.blessingMargin);

        TextView sub_blessing = view.findViewById(R.id.sub_blessing);
        sub_blessing.setText(holyDay.subBlessing);
        sub_blessing.setTextColor(holyDay.blessingColor);
    }

    public HolyDaysFragment() {
    }

    public static AlertDialog.Builder setDialog(AlertDialog.Builder alertDialogBuilder, Activity activity) {


        final Spinner holydays = new Spinner(activity);
        List<String> holyDayNames = getHolyDayNames();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, holyDayNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holydays.setAdapter(adapter);
        holydays.setId(DIALOG_HOLY_DAY_ID);
        alertDialogBuilder.setView(holydays);


        return alertDialogBuilder;

    }


    public static List<String> getHolyDayNames() {
        List<String> holyDayNames = new ArrayList<>();

        for (HolyDay holyDay : HOLY_DAYS) {
            holyDayNames.add(holyDay.blessing);
        }

        return holyDayNames;
    }


    public static HolyDay getHolyDayFromDialog(AlertDialog dialog) {
        Spinner holydaysSpinner = dialog.findViewById(DIALOG_HOLY_DAY_ID);

        // Get the selected position from the Spinner
        int selectedPosition = holydaysSpinner.getSelectedItemPosition();

        if (selectedPosition >= 0 && selectedPosition < HOLY_DAYS.length) {
            // Return the selected HolyDay
            return HOLY_DAYS[selectedPosition];
        } else {
            // Handle the case where no holy day is selected
            return null;
        }
    }


    public static SlideFragment getInstantFromDialog(AlertDialog dialog) {
        HolyDay selectedHolyDay = HolyDaysFragment.getHolyDayFromDialog(dialog);
        return new HolyDaysFragment(selectedHolyDay);
    }

    public HolyDay getHolyDay() {
        return holyDay;
    }

    public void setHolyDay(HolyDay holyDay) {
        this.holyDay = holyDay;
    }

    public static int getDialogHolyDayId() {
        return DIALOG_HOLY_DAY_ID;
    }

    public static void setDialogHolyDayId(int dialogHolyDayId) {
        DIALOG_HOLY_DAY_ID = dialogHolyDayId;
    }
}
