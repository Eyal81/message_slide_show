package com.eyalcorp.message_slide_show.Slides;

import static com.eyalcorp.message_slide_show.Zmanim.CityInfo.getCities;
import static com.eyalcorp.message_slide_show.Zmanim.CityInfo.getCityInfo;
import static com.eyalcorp.message_slide_show.Zmanim.ZmanimCalculator.calculateSunriseOhrHaChaim;
import static com.eyalcorp.message_slide_show.Zmanim.ZmanimCalculator.calculateSunsetOhrHaChaim;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.eyalcorp.message_slide_show.Zmanim.CityInfo;
import com.eyalcorp.messageslideshow.R;

import java.util.List;


public class ZmanimSlideFragment extends SlideFragment {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 123;

    private final String cityName;

    private TextView sunriseTimeTextView;
    private TextView sunsetTimeTextView;

    public ZmanimSlideFragment(String cityName) {
        this.cityName = cityName;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_zmanim_slide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sunriseTimeTextView = view.findViewById(R.id.sunriseTimeTextView);
        sunsetTimeTextView = view.findViewById(R.id.sunsetTimeTextView);

        // Check location permissions
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            getLocationAndCalculateZmanim();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLocationAndCalculateZmanim();
            }
        }
    }

    private void getLocationAndCalculateZmanim() {
        CityInfo.CityData cityInfo = getCityInfo(cityName);
        sunriseTimeTextView.setText(calculateSunriseOhrHaChaim(
                cityInfo.getLatitude(), cityInfo.getLongitude()));

        sunsetTimeTextView.setText(calculateSunsetOhrHaChaim(
                cityInfo.getLatitude(), cityInfo.getLongitude()));

    }

    public String getCityName() {
        return cityName;
    }

    private static final int DIALOG_CITY_ID = View.generateViewId();

    public ZmanimSlideFragment() {
        cityName = "";
    }

    public static AlertDialog.Builder setDialog(AlertDialog.Builder alertDialogBuilder, Activity activity) {


        final Spinner cities = new Spinner(activity);
        List<String> holyDayNames = getCities();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, holyDayNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cities.setAdapter(adapter);
        cities.setId(DIALOG_CITY_ID);
        alertDialogBuilder.setView(cities);

        return alertDialogBuilder;


    }

    public static SlideFragment getInstantFromDialog(AlertDialog dialog) {
        Spinner cities = dialog.findViewById(DIALOG_CITY_ID);
        int selectedPosition = cities.getSelectedItemPosition();
        return new ZmanimSlideFragment(getCities().get(selectedPosition));
    }

}