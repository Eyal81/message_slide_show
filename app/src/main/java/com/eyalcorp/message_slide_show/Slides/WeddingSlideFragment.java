package com.eyalcorp.message_slide_show.Slides;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.eyalcorp.messageslideshow.R;

public class WeddingSlideFragment extends SlideFragment {

    public String brideName;

    public WeddingSlideFragment(String brideName) {
        this.brideName = brideName;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.wedding_fragment_slide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((TextView) view.findViewById(R.id.bride_name)).setText(brideName);
    }

    public WeddingSlideFragment() {
    }

    private static final int DIALOG_BRIDE_ID = View.generateViewId();

    public static AlertDialog.Builder setDialog(AlertDialog.Builder alertDialogBuilder, Activity activity) {


        final EditText bride = new EditText(activity);
        bride.setHint("Bride name");
        bride.setId(DIALOG_BRIDE_ID);
        alertDialogBuilder.setView(bride);


        return alertDialogBuilder;


    }

    public static SlideFragment getInstantFromDialog(AlertDialog dialog) {
        EditText bride = dialog.findViewById(DIALOG_BRIDE_ID);

        return new WeddingSlideFragment(bride.getText().toString());
    }
}
