package com.eyalcorp.message_slide_show.Slides;



import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.eyalcorp.messageslideshow.R;

// .java
public class BirthdaySlideFragment extends SlideFragment {

    public String childName;
    public String birthdayDate;

    public static int DIALOG_NAME_ID = View.generateViewId();
    public static int DIALOG_DATE_ID = View.generateViewId();


    public BirthdaySlideFragment() {
    }

    public BirthdaySlideFragment(String childName, String birthdayDate) {
        this.childName = childName;
        this.birthdayDate = birthdayDate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.birthday_fragment_slide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((TextView) view.findViewById(R.id.birthday_child_name)).setText(childName);
        ((TextView) view.findViewById(R.id.birthday_child_date)).setText(birthdayDate);
    }


    public static AlertDialog.Builder setDialog(AlertDialog.Builder alertDialogBuilder,
                                                Activity activity) {


        final EditText cName = new EditText(activity);
        cName.setHint("Child Name");
        cName.setId(DIALOG_NAME_ID);

        final EditText cDate = new EditText(activity);
        cDate.setId(DIALOG_DATE_ID);
        cDate.setHint("birthdayDate");

        // Create a parent layout (e.g., LinearLayout) to contain cName and cDate
        LinearLayout container = new LinearLayout(activity);
        container.setOrientation(LinearLayout.VERTICAL);

        // Add cName and cDate to the parent layout
        container.addView(cName);
        container.addView(cDate);

        // Set the parent layout as the view for the AlertDialog
        alertDialogBuilder.setView(container);


        return alertDialogBuilder;


    }

    public static SlideFragment getInstantFromDialog(AlertDialog dialog) {
        EditText cName = dialog.findViewById(DIALOG_NAME_ID);
        EditText cDate = dialog.findViewById(DIALOG_DATE_ID);

        return new BirthdaySlideFragment(cName.getText().toString(),
                cDate.getText().toString());
    }

}
