package com.eyalcorp.message_slide_show.Slides;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.eyalcorp.messageslideshow.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

// .java
public class DefamationLawsSlideFragment extends SlideFragment {


    private TextView quoteTextView;
    private TextView titleTextView;

    private JSONObject quotesObject;
    private String[] titleKeys;
    private int currentTitleIndex = 0;
    private int currentQuoteIndex = 0;
    private String lastDisplayedDate = "";

    public DefamationLawsSlideFragment() {
        this.SlideTimeMS = 20000;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.defamation_laws_fragment_slide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        quoteTextView = view.findViewById(R.id.text);
        titleTextView = view.findViewById(R.id.title);

        // Load quotes from assets folder
        loadQuotesFromJSON();
        displayNextQuote();
        lastDisplayedDate = getCurrentDate();

    }

    private void loadQuotesFromJSON() {
        try {
            InputStream inputStream = getActivity().getAssets().open("chofetz_chaim.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            String json = new String(buffer, "UTF-8");

            JSONObject jsonObject = new JSONObject(json);
            quotesObject = jsonObject;

            // Get an array of keys from the JSON object
            titleKeys = getJsonKeys(quotesObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String[] getJsonKeys(JSONObject jsonObject) {
        String[] keys = new String[jsonObject.length()];
        Iterator<String> iterator = jsonObject.keys();
        int index = 0;
        while (iterator.hasNext()) {
            keys[index++] = iterator.next();
        }
        return keys;
    }


    private void displayNextQuote() {
        if (quotesObject != null && titleKeys != null && titleKeys.length > 0) {
            String currentTitleKey = titleKeys[currentTitleIndex];
            JSONArray currentTitleQuotes = quotesObject.optJSONArray(currentTitleKey);

            if (currentTitleQuotes != null) {
                if (currentQuoteIndex >= currentTitleQuotes.length()) {
                    currentQuoteIndex = 0;
                    currentTitleIndex = (currentTitleIndex + 1) % titleKeys.length;
                    displayNextQuote(); // Recursive call to move to the next title
                    return;
                }

                try {
                    String quoteText = currentTitleQuotes.getString(currentQuoteIndex);
                    String titleText = "הלכות לשון הרע - " +  currentTitleKey;

                    // Update the TextViews to display the title and quote
                    quoteTextView.setText(quoteText);
                    titleTextView.setText(titleText);

                    currentQuoteIndex++;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date());
    }
}
