package com.eyalcorp.message_slide_show.Slides;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.eyalcorp.messageslideshow.R;

public class NoteSlideFragment extends SlideFragment {

    private static final int DIALOG_TITLE_ID = View.generateViewId();
    private static final int DIALOG_TEXT_ID = View.generateViewId();
    public String title;
    public String text;

    public NoteSlideFragment(String title, String text) {
        this.title = title;
        this.text = text;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.note_fragment_slide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((TextView) view.findViewById(R.id.title)).setText(title);
        ((TextView) view.findViewById(R.id.text)).setText(text);
    }

    public NoteSlideFragment() {
    }

    public static AlertDialog.Builder setDialog(AlertDialog.Builder alertDialogBuilder, Activity activity) {

        final EditText title = new EditText(activity);
        title.setHint("Title");
        title.setId(DIALOG_TITLE_ID);

        final EditText text = new EditText(activity);
        text.setId(DIALOG_TEXT_ID);
        text.setHint("Text");

        // Create a parent layout (e.g., LinearLayout) to contain title and text
        LinearLayout container = new LinearLayout(activity);
        container.setOrientation(LinearLayout.VERTICAL);

        // Add title and text to the parent layout
        container.addView(title);
        container.addView(text);


        // Set the parent layout as the view for the AlertDialog
        alertDialogBuilder.setView(container);

        return alertDialogBuilder;


    }

    public static SlideFragment getInstantFromDialog(AlertDialog dialog) {
        EditText title = dialog.findViewById(DIALOG_TITLE_ID);
        EditText text = dialog.findViewById(DIALOG_TEXT_ID);

        return new NoteSlideFragment(title.getText().toString(),
                text.getText().toString());
    }
}
