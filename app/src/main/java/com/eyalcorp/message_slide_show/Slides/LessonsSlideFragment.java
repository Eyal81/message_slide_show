package com.eyalcorp.message_slide_show.Slides;

import static android.app.Activity.RESULT_OK;
import static com.eyalcorp.message_slide_show.FilePathHelper.getRealPathFromURI;
import static com.eyalcorp.message_slide_show.Utils.REQUEST_PICK_IMAGE;
import static com.eyalcorp.message_slide_show.Utils.pickImage;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.eyalcorp.message_slide_show.SettingsActivity;
import com.eyalcorp.messageslideshow.R;
import com.skydoves.colorpickerview.ColorPickerView;

public class LessonsSlideFragment extends SlideFragment {
    public String lessonText;

    public int textColor;

    public MsgPos msgPos;
    public String backgroundImagePath;

    private static String imageSelected;

    public enum MsgPos {
        LEFT,
        CENTER,
        RIGHT
    }


    public LessonsSlideFragment(String lessonText, int textColor, MsgPos msgPos, String backgroundImagePath) {
        this.lessonText = lessonText;
        this.textColor = textColor;
        this.msgPos = msgPos;
        this.backgroundImagePath = backgroundImagePath;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.lessons_fragment_slide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayout lessonLayout = view.findViewById(R.id.lesson_layout);

        if (backgroundImagePath != null && !backgroundImagePath.isEmpty()) {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 123);
            } else {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;

                Bitmap backgroundBitmap = BitmapFactory.decodeFile(backgroundImagePath,options);
                Drawable backgroundDrawable = new BitmapDrawable(getResources(), backgroundBitmap);
                lessonLayout.setBackground(backgroundDrawable);
            }

        } else {
            // Handle the case when the backgroundImagePath is empty or null
            lessonLayout.setBackgroundColor(Color.WHITE);
            // Set a default background resource
        }

        ViewGroup selectedLayout = msgPos == MsgPos.CENTER ? view.findViewById(R.id.centerLayout)
                : msgPos == MsgPos.LEFT ? view.findViewById(R.id.leftLayout) : view.findViewById(R.id.rightLayout);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View lessonItemLayout = inflater.inflate(R.layout.lessons_text,
                selectedLayout, false);
        selectedLayout.addView(lessonItemLayout);
        ((TextView) lessonItemLayout.findViewById(R.id.lesson_text)).setText(lessonText);
        ((TextView) lessonItemLayout.findViewById(R.id.lesson_text)).setTextColor(textColor);
    }

    public LessonsSlideFragment() {
    }

    public static AlertDialog.Builder setDialog(AlertDialog.Builder alertDialogBuilder,
                                                Activity activity) {

        LayoutInflater inflater = LayoutInflater.from(activity);
        View dialogView = inflater.inflate(R.layout.lessons_dialog_layout, null);
        alertDialogBuilder.setView(dialogView);

        final Button btnSelectImage = dialogView.findViewById(R.id.btnSelectImage);
        btnSelectImage.setOnClickListener(view ->
        {

            ((SettingsActivity) activity).setActivityResultListener((requestCode, resultCode, data) -> {
                if (requestCode == REQUEST_PICK_IMAGE && resultCode == RESULT_OK && data != null) {
                    // User has selected an image
                    Uri selectedImageUri = data.getData();

                    // Convert the selected image URI to a Bitmap
                    imageSelected = getRealPathFromURI(activity, selectedImageUri);
                }
            });
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted, so request it from the user
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PICK_IMAGE);
            } else {
                // Permission is already granted, proceed to open the file picker
                pickImage(activity);
            }


        });

        return alertDialogBuilder;

    }


    public static SlideFragment getInstantFromDialog(AlertDialog dialog) {
        final EditText editTextLessonText = dialog.findViewById(R.id.editTextLessonText);
        final ColorPickerView colorPickerView = dialog.findViewById(R.id.colorPickerView);
        final Spinner spinnerMsgPos = dialog.findViewById(R.id.spinnerMsgPos);


        int selectedPosition = spinnerMsgPos.getSelectedItemPosition();
        String selectedValue = dialog.getContext().getResources().getStringArray(R.array.msg_positions)[selectedPosition];
        MsgPos selectedMsgPos = MsgPos.valueOf(selectedValue);


        return new LessonsSlideFragment(editTextLessonText.getText().toString(),
                colorPickerView.getColor(), selectedMsgPos, imageSelected);
    }
}
