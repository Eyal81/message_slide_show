package com.eyalcorp.message_slide_show;

import android.content.Intent;

import androidx.annotation.Nullable;

public interface ActivityResultListener {
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data);
}
