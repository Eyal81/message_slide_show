package com.eyalcorp.message_slide_show;

import com.eyalcorp.message_slide_show.Slides.BirthdaySlideFragment;
import com.eyalcorp.message_slide_show.Slides.HolyDaysFragment;
import com.eyalcorp.message_slide_show.Slides.LessonsSlideFragment;
import com.eyalcorp.message_slide_show.Slides.NoteSlideFragment;
import com.eyalcorp.message_slide_show.Slides.SlideFragment;
import com.eyalcorp.message_slide_show.Slides.WeddingSlideFragment;
import com.eyalcorp.message_slide_show.Slides.ZmanimSlideFragment;

import java.util.HashMap;
import java.util.Map;

public class SlideDialogManager {
    private static final Map<String, Class<? extends SlideFragment>> classMap = new HashMap<>();

    static {
        classMap.put("BirthdaySlideFragment", BirthdaySlideFragment.class);
        classMap.put("HolyDaysFragment", HolyDaysFragment.class);
        classMap.put("LessonsSlideFragment", LessonsSlideFragment.class);
        classMap.put("NoteSlideFragment", NoteSlideFragment.class);
        classMap.put("WeddingSlideFragment", WeddingSlideFragment.class);
        classMap.put("ZmanimSlideFragment", ZmanimSlideFragment.class);
    }

    public static Class<? extends SlideFragment> getClassForName(String className) {
        return classMap.get(className);
    }
}
