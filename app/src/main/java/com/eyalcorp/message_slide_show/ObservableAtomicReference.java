package com.eyalcorp.message_slide_show;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class ObservableAtomicReference<T> {
    private final AtomicReference<T> atomicReference;
    private Consumer<T> onSetListener;

    public ObservableAtomicReference() {
        atomicReference = new AtomicReference<>();
    }

    public ObservableAtomicReference(T initialValue) {
        atomicReference = new AtomicReference<>(initialValue);
    }

    public void setOnSetListener(Consumer<T> listener) {
        this.onSetListener = listener;
    }

    public void set(T newValue) {
        atomicReference.set(newValue);

        // Notify the listener if it's set
        if (onSetListener != null) {
            onSetListener.accept(newValue);
        }
    }

    public T get() {
        return atomicReference.get();
    }
}
