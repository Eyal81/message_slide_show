package com.eyalcorp.message_slide_show;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.eyalcorp.message_slide_show.Slides.SlideFragment;
import com.eyalcorp.messageslideshow.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private final Handler handler = new Handler();
    private ViewPager2 viewPager;

    private static final int SLIDE_TIME = 5000; // 5 seconds

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.viewPager);

        // Initialize fragments for slides

        // Create a FragmentPersistenceHelper for your slide fragments
        Type fragmentDataType = new TypeToken<ArrayList<SlideFragmentData>>() {}.getType();
        FragmentPersistenceHelper<ArrayList<SlideFragmentData>> fragmentPersistenceHelper =
                new FragmentPersistenceHelper<>(this, fragmentDataType);

        ArrayList<SlideFragmentData> loadedFragmentDataList = fragmentPersistenceHelper.loadFragments();
        if (loadedFragmentDataList == null) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            this.finish();
            return;
        }

        ArrayList<SlideFragment> loadedSlides = new ArrayList<>();
        Gson gson = new Gson();
        for (SlideFragmentData fragmentData : loadedFragmentDataList) {
            try {
                Class<?> fragmentClass = Class.forName(fragmentData.type);
                SlideFragment slide = (SlideFragment) gson.fromJson(fragmentData.argsJson, fragmentClass);
                loadedSlides.add(slide);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }



        ((MyApplication) this.getApplication()).setSlides(loadedSlides);
        // Add other slide fragments as needed

        FragmentsPagerAdapter fragmentsPagerAdapter = new FragmentsPagerAdapter(getSupportFragmentManager(), getLifecycle(), loadedSlides);
        viewPager.setAdapter(fragmentsPagerAdapter);


        viewPager.setPageTransformer(new ZoomOutTransformer());

        // Preload adjacent fragments to reduce the white screen pause
        viewPager.setOffscreenPageLimit(2); // Set the number of adjacent fragments to preload

        startSlideshow(fragmentsPagerAdapter);
    }

    private void startSlideshow(FragmentsPagerAdapter fragmentsPagerAdapter) {
        // Create a Runnable to switch slide every SLIDE_TIME milliseconds
        Runnable runnable = new Runnable() {
            private int currentPage = 0;
            boolean op = true;

            @Override
            public void run() {
                int pageCount = fragmentsPagerAdapter.getItemCount();
                currentPage = op ? currentPage + 1 : currentPage - 1;
                op = (currentPage == pageCount || currentPage == 0) != op;
                viewPager.setCurrentItem(currentPage, true);
                int slideTime = ((SlideFragment) getSupportFragmentManager().findFragmentByTag("f" + currentPage)) != null ?
                        ((SlideFragment) getSupportFragmentManager().findFragmentByTag("f" + currentPage)).getSlideTimeMS() : SLIDE_TIME;
                // Schedule the next slide transition
                handler.postDelayed(this, slideTime);
            }
        };

        int slideTime = ((SlideFragment) getSupportFragmentManager().findFragmentByTag("f" + 0)) != null ?
                ((SlideFragment) getSupportFragmentManager().findFragmentByTag("f" + 0)).getSlideTimeMS() : SLIDE_TIME;

        // Start the slideshow by posting the initial runnable
        handler.postDelayed(runnable, slideTime);
    }


}
