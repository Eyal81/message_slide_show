package com.eyalcorp.message_slide_show;

import static com.eyalcorp.message_slide_show.Utils.isInstanceOfAny;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.eyalcorp.message_slide_show.Slides.DefamationLawsSlideFragment;
import com.eyalcorp.message_slide_show.Slides.InstarabbiappSlideFragment;
import com.eyalcorp.message_slide_show.Slides.MatchmakingSlideFragment;
import com.eyalcorp.message_slide_show.Slides.PhoneNumbersSlideFragment;
import com.eyalcorp.message_slide_show.Slides.SlideFragment;
import com.eyalcorp.message_slide_show.Slides.WelcomeSlideFragment;
import com.eyalcorp.messageslideshow.R;

import java.util.ArrayList;
import java.util.Collections;

public class SlideListAdapter extends ArrayAdapter<SlideFragment> {

    private final Context context;
    private final ArrayList<SlideFragment> fragments;


    private static final
    Class<?>[] defaultSlides =

            {
                    WelcomeSlideFragment.class,
                    MatchmakingSlideFragment.class,
                    DefamationLawsSlideFragment.class,
                    InstarabbiappSlideFragment.class,
                    PhoneNumbersSlideFragment.class};


    public SlideListAdapter(Context context, ArrayList<SlideFragment> fragments) {
        super(context, 0, fragments);
        this.context = context;
        this.fragments = fragments;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        SlideFragment fragment = fragments.get(position);
        if (convertView == null && isInstanceOfAny(fragment.getClass(), defaultSlides)) {
            convertView = LayoutInflater.from(context).inflate(R.layout.simple_list_item_slide, parent, false);
        } else if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.removable_list_item_slide, parent, false);

        }

        MyApplication myApp = (MyApplication) getContext().getApplicationContext();
        if (fragment != null) {

            TextView slideName = convertView.findViewById(R.id.slideName);
            slideName.setText(fragment.getClass().getSimpleName());

            ImageButton btnUp = convertView.findViewById(R.id.btnUp);
            ImageButton btnDown = convertView.findViewById(R.id.btnDown);


            btnUp.setOnClickListener(v -> {
                if (position > 0) {
                    // Create copies of both lists
                    ArrayList<SlideFragment> fragmentsCopy = new ArrayList<>(fragments);
                    ArrayList<SlideFragment> myAppFragmentsCopy = new ArrayList<>(myApp.getSlides());

                    // Swap the items in both copies
                    Collections.swap(fragmentsCopy, position, position - 1);
                    Collections.swap(myAppFragmentsCopy, position, position - 1);

                    // Update the original lists with the copies
                    fragments.clear();
                    fragments.addAll(fragmentsCopy);
                    myApp.getSlides().clear();
                    myApp.getSlides().addAll(myAppFragmentsCopy);

                    notifyDataSetChanged(); // Immediately notify the adapter
                }
            });

            btnDown.setOnClickListener(v -> {
                if (position < fragments.size() - 1) {
                    // Create copies of both lists
                    ArrayList<SlideFragment> fragmentsCopy = new ArrayList<>(fragments);
                    ArrayList<SlideFragment> myAppFragmentsCopy = new ArrayList<>(myApp.getSlides());

                    // Swap the items in both copies
                    Collections.swap(fragmentsCopy, position, position + 1);
                    Collections.swap(myAppFragmentsCopy, position, position + 1);

                    // Update the original lists with the copies
                    fragments.clear();
                    fragments.addAll(fragmentsCopy);
                    myApp.getSlides().clear();
                    myApp.getSlides().addAll(myAppFragmentsCopy);

                    notifyDataSetChanged(); // Immediately notify the adapter
                }
            });


            if (!isInstanceOfAny(fragment.getClass(), defaultSlides)) {
                ImageButton btnDelete = convertView.findViewById(R.id.btnDel);

                // Check if the delete button is not null before setting the click listener
                if (btnDelete != null) {
                    btnDelete.setOnClickListener(v -> {
                        // Check if the position is within valid bounds
                        if (position >= 0 && position < fragments.size() && position < myApp.getSlides().size()) {
                            // Create a copy of the fragments list
                            ArrayList<SlideFragment> fragmentsCopy = new ArrayList<>(fragments);
                            ArrayList<SlideFragment> myAppFragmentsCopy = new ArrayList<>(myApp.getSlides());

                            // Remove the fragment from both copies using the captured position
                            fragmentsCopy.remove(position);
                            myAppFragmentsCopy.remove(position);

                            // Update the original lists with the copies
                            fragments.clear();
                            fragments.addAll(fragmentsCopy);
                            myApp.getSlides().clear();
                            myApp.getSlides().addAll(myAppFragmentsCopy);

                            notifyDataSetChanged(); // Immediately notify the adapter
                        }
                    });
                }
            }



        }

        return convertView;
    }


}
