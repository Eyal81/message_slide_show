package com.eyalcorp.message_slide_show;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;

import java.io.ByteArrayOutputStream;

public class Utils {

    public static final int REQUEST_PICK_IMAGE = 1; // Define a request code


    public static class Margin {
        public int l, t, r, b;

        public Margin(int l, int t, int r, int b) {
            this.l = l;
            this.t = t;
            this.r = r;
            this.b = b;
        }

        public Margin() {
        }
    }

    public static void setMargins(View v, Margin m) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(m.l, m.t, m.r, m.b);
            v.requestLayout();
        }
    }

    public static boolean isInstanceOfAny(Class<?> objClass, Class<?>[] arrayOfClasses) {
        boolean isInstanceOfAny = false;

        for (Class<?> clazz : arrayOfClasses) {
            if (objClass == clazz) {
                isInstanceOfAny = true;
                break; // No need to continue checking once a match is found
            }
        }
        return isInstanceOfAny;
    }

    public static void pickImage(Activity a) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*"); // Allow only image files
        a.startActivityForResult(intent, REQUEST_PICK_IMAGE);
    }


    // Convert bytes to Bitmap
    public static Bitmap bytesToBitmap(byte[] byteArray) {
        if (byteArray == null) {
            return null;
        }
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }

    // Convert Bitmap to bytes




}
