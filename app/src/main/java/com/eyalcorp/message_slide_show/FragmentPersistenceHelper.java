package com.eyalcorp.message_slide_show;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public class FragmentPersistenceHelper<T> {
    private SharedPreferences sharedPreferences;
    private Gson gson;
    private Type type;

    public FragmentPersistenceHelper(Context context, Type type) {
        sharedPreferences = context.getSharedPreferences("fragment_data", Context.MODE_PRIVATE);
        gson = new Gson();
        this.type = type;
    }

    public void saveFragments(T fragments) {
        String json = gson.toJson(fragments);
         sharedPreferences.edit().putString("fragments", json).apply();
    }

    public T loadFragments() {
        String json = sharedPreferences.getString("fragments", null);
        if (json != null) {
            return gson.fromJson(json, type);
        }
        return null;
    }
}
