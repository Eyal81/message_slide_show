package com.eyalcorp.message_slide_show;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.eyalcorp.message_slide_show.Slides.SlideFragment;

import java.util.ArrayList;

public class FragmentsPagerAdapter extends FragmentStateAdapter {

    private final ArrayList<SlideFragment> fragments;

    public FragmentsPagerAdapter(FragmentManager fragmentManager, Lifecycle lifecycle, ArrayList<SlideFragment> fragments) {
        super(fragmentManager, lifecycle);
        this.fragments = fragments;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        position %= fragments.size();
        return fragments.get(position);
    }

    @Override
    public int getItemCount() {
        return fragments.size();
    }
}
