package com.eyalcorp.message_slide_show;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.eyalcorp.message_slide_show.Serialization.BirthdaySlideFragmentSerializer;
import com.eyalcorp.message_slide_show.Serialization.DefamationLawsSlideFragmentSerializer;
import com.eyalcorp.message_slide_show.Serialization.HolyDaysFragmentSerializer;
import com.eyalcorp.message_slide_show.Serialization.InstarabbiappSlideFragmentSerializer;
import com.eyalcorp.message_slide_show.Serialization.LessonsSlideFragmentSerializer;
import com.eyalcorp.message_slide_show.Serialization.MatchmakingSlideFragmentSerializer;
import com.eyalcorp.message_slide_show.Serialization.NoteSlideFragmentSerializer;
import com.eyalcorp.message_slide_show.Serialization.PhoneNumbersSlideFragmentSerializer;
import com.eyalcorp.message_slide_show.Serialization.WeddingSlideFragmentSerializer;
import com.eyalcorp.message_slide_show.Serialization.WelcomeSlideFragmentSerializer;
import com.eyalcorp.message_slide_show.Serialization.ZmanimSlideFragmentSerializer;
import com.eyalcorp.message_slide_show.Slides.BirthdaySlideFragment;
import com.eyalcorp.message_slide_show.Slides.DefamationLawsSlideFragment;
import com.eyalcorp.message_slide_show.Slides.HolyDaysFragment;
import com.eyalcorp.message_slide_show.Slides.InstarabbiappSlideFragment;
import com.eyalcorp.message_slide_show.Slides.LessonsSlideFragment;
import com.eyalcorp.message_slide_show.Slides.MatchmakingSlideFragment;
import com.eyalcorp.message_slide_show.Slides.NoteSlideFragment;
import com.eyalcorp.message_slide_show.Slides.PhoneNumbersSlideFragment;
import com.eyalcorp.message_slide_show.Slides.SlideFragment;
import com.eyalcorp.message_slide_show.Slides.WeddingSlideFragment;
import com.eyalcorp.message_slide_show.Slides.WelcomeSlideFragment;
import com.eyalcorp.message_slide_show.Slides.ZmanimSlideFragment;
import com.eyalcorp.messageslideshow.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class SettingsActivity extends AppCompatActivity {

    private ListView slideListView;

    private ActivityResultListener activityResultListener;

    public ActivityResultListener getActivityResultListener() {
        return activityResultListener;
    }

    public void setActivityResultListener(ActivityResultListener activityResultListener) {
        this.activityResultListener = activityResultListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        slideListView = findViewById(R.id.slideListView);

        MyApplication myApp = (MyApplication) getApplication();
        ArrayList<SlideFragment> slides = myApp.getSlides();
        if (slides.isEmpty()) {

            slides.add(new WelcomeSlideFragment());
            slides.add(new MatchmakingSlideFragment());
            slides.add(new InstarabbiappSlideFragment());
            slides.add(new DefamationLawsSlideFragment());
            slides.add(new PhoneNumbersSlideFragment());


        }

        SlideListAdapter adapter = new SlideListAdapter(this, slides);
        slideListView.setAdapter(adapter);
    }


    public void onAddSlideButtonClick(View view) {
        showAddSlideDialog();
    }


    private void showAddSlideDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add_slide, null);

        Spinner slideTypeSpinner = dialogView.findViewById(R.id.slideTypeSpinner);

        builder.setView(dialogView)
                .setTitle("Add Slide")
                .setPositiveButton("Add", (dialog, which) -> {
                    String selectedSlideType = slideTypeSpinner.getSelectedItem().toString();
                    Class<? extends SlideFragment> slideClass = SlideDialogManager.getClassForName(selectedSlideType);

                    if (slideClass != null) {
                        // Create an instance of the selected class
                        // Create an AlertDialog.Builder
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SettingsActivity.this);
                        AtomicReference<AlertDialog> alertDialog = new AtomicReference<>(alertDialogBuilder.create());
                        alertDialogBuilder.setTitle("Slide Settings").setNegativeButton("Cancel", null);

                        ObservableAtomicReference<SlideFragment> newSlideFragment = new ObservableAtomicReference<>();
                        newSlideFragment.setOnSetListener(slideFragment -> {
                            //Add to fragment list
                            MyApplication myApp = (MyApplication) getApplication();
                            myApp.getSlides().add(slideFragment);

                            SlideListAdapter adapter = new SlideListAdapter(this, myApp.getSlides());
                            slideListView.setAdapter(adapter);

                        });

                        // Handle each class separately and call its setDialog method
                        if (slideClass.equals(HolyDaysFragment.class)) {
                            HolyDaysFragment holyDaysFragment = new HolyDaysFragment();
                            alertDialogBuilder = holyDaysFragment.setDialog(alertDialogBuilder, SettingsActivity.this);
                            alertDialogBuilder.setPositiveButton("Add", (dialogInterface, i) ->
                                    newSlideFragment.set(holyDaysFragment.getInstantFromDialog(alertDialog.get())));

                        } else if (slideClass.equals(BirthdaySlideFragment.class)) {
                            BirthdaySlideFragment birthdaySlideFragment = new BirthdaySlideFragment();
                            alertDialogBuilder = birthdaySlideFragment.setDialog(alertDialogBuilder, SettingsActivity.this);
                            alertDialogBuilder.setPositiveButton("Add", (dialogInterface, i) ->
                                    newSlideFragment.set(birthdaySlideFragment.getInstantFromDialog(alertDialog.get())));
                        } else if (slideClass.equals(LessonsSlideFragment.class)) {
                            LessonsSlideFragment lessonsSlideFragment = new LessonsSlideFragment();
                            alertDialogBuilder = lessonsSlideFragment.setDialog(alertDialogBuilder, SettingsActivity.this);
                            alertDialogBuilder.setPositiveButton("Add", (dialogInterface, i) ->
                                    newSlideFragment.set(lessonsSlideFragment.getInstantFromDialog(alertDialog.get())));
                        } else if (slideClass.equals(NoteSlideFragment.class)) {
                            NoteSlideFragment noteSlideFragment = new NoteSlideFragment();
                            alertDialogBuilder = noteSlideFragment.setDialog(alertDialogBuilder, SettingsActivity.this);
                            alertDialogBuilder.setPositiveButton("Add", (dialogInterface, i) ->
                                    newSlideFragment.set(noteSlideFragment.getInstantFromDialog(alertDialog.get())));
                        } else if (slideClass.equals(WeddingSlideFragment.class)) {
                            WeddingSlideFragment weddingSlideFragment = new WeddingSlideFragment();
                            alertDialogBuilder = weddingSlideFragment.setDialog(alertDialogBuilder, SettingsActivity.this);
                            alertDialogBuilder.setPositiveButton("Add", (dialogInterface, i) ->
                                    newSlideFragment.set(weddingSlideFragment.getInstantFromDialog(alertDialog.get())));
                        } else if (slideClass.equals(ZmanimSlideFragment.class)) {
                            ZmanimSlideFragment zmanimSlideFragment = new ZmanimSlideFragment();
                            alertDialogBuilder = zmanimSlideFragment.setDialog(alertDialogBuilder, SettingsActivity.this);
                            alertDialogBuilder.setPositiveButton("Add", (dialogInterface, i) ->
                                    newSlideFragment.set(zmanimSlideFragment.getInstantFromDialog(alertDialog.get())));
                        }

                        // Show the AlertDialog
                        alertDialog.set(alertDialogBuilder.show());

                    }
                })
                .setNegativeButton("Cancel", null)
                .create()
                .show();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        activityResultListener.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        MyApplication myApp = (MyApplication) getApplication();
        ArrayList<SlideFragment> slides = myApp.getSlides();

        Type fragmentDataType = new TypeToken<ArrayList<SlideFragmentData>>() {
        }.getType();
        FragmentPersistenceHelper<ArrayList<SlideFragmentData>> fragmentPersistenceHelper =
                new FragmentPersistenceHelper<>(this, fragmentDataType);

        // Save your slide fragments
        ArrayList<SlideFragmentData> fragmentDataList = new ArrayList<>();

        GsonBuilder gsonBuilder = new GsonBuilder();

        // Register custom serializers for slide fragments
        gsonBuilder.registerTypeAdapter(BirthdaySlideFragment.class, new BirthdaySlideFragmentSerializer());
        gsonBuilder.registerTypeAdapter(MatchmakingSlideFragment.class, new MatchmakingSlideFragmentSerializer());
        gsonBuilder.registerTypeAdapter(DefamationLawsSlideFragment.class, new DefamationLawsSlideFragmentSerializer());
        gsonBuilder.registerTypeAdapter(HolyDaysFragment.class, new HolyDaysFragmentSerializer());
        gsonBuilder.registerTypeAdapter(InstarabbiappSlideFragment.class, new InstarabbiappSlideFragmentSerializer());
        gsonBuilder.registerTypeAdapter(LessonsSlideFragment.class, new LessonsSlideFragmentSerializer());
        gsonBuilder.registerTypeAdapter(NoteSlideFragment.class, new NoteSlideFragmentSerializer());
        gsonBuilder.registerTypeAdapter(PhoneNumbersSlideFragment.class, new PhoneNumbersSlideFragmentSerializer());
        gsonBuilder.registerTypeAdapter(WeddingSlideFragment.class, new WeddingSlideFragmentSerializer());
        gsonBuilder.registerTypeAdapter(WelcomeSlideFragment.class, new WelcomeSlideFragmentSerializer());
        gsonBuilder.registerTypeAdapter(ZmanimSlideFragment.class, new ZmanimSlideFragmentSerializer());
        Gson gson = gsonBuilder.create();

        for (SlideFragment slide : slides) {
            SlideFragmentData fragmentData = new SlideFragmentData();
            fragmentData.type = slide.getClass().getName();
            fragmentData.argsJson = gson.toJson(slide);
            fragmentDataList.add(fragmentData);
        }
        fragmentPersistenceHelper.saveFragments(fragmentDataList);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        this.finish();
    }
}
